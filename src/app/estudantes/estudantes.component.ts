import { Component, OnInit, NgModule } from '@angular/core';
import { Location } from '@angular/common';
import { environment } from './../../environments/environment';
import { FormControl, FormGroup, Validators } from '@angular/forms';

declare const Parse: any;

@Component({
  selector: 'app-estudantes',
  templateUrl: './estudantes.component.html',
  styleUrls: ['./estudantes.component.css']
})
export class EstudantesComponent implements OnInit {

  estudante = { id: '', nome: '', sobrenome: '', email: '' };

  Estudantes = [{
    id: '',
    nome: '',
    sobrenome: '',
    email: ''
  }];

  estudanteForm: FormGroup;

  ngOnInit(): void {
    this.estudanteForm = new FormGroup({
      'nome': new FormControl(this.estudante.nome, [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(100)
      ]),
      'sobrenome': new FormControl(this.estudante.sobrenome, [
        Validators.required
      ]),
      'email': new FormControl(this.estudante.email, [
        Validators.required,
        Validators.email
      ])
    });
  }

  constructor(private location: Location) {
    Parse.initialize(environment.PARSE_APP_ID, environment.PARSE_JS_KEY);
    Parse.serverURL = environment.serverURL;
    this.Estudantes = this.listaEstudantes();
  }

  get nome() { return this.estudanteForm.get('nome'); }
  get sobrenome() { return this.estudanteForm.get('sobrenome'); }
  get email() { return this.estudanteForm.get('email'); }

  listaEstudantes() {
    const aux = [];
    const Estudante = Parse.Object.extend('cadastroEstudantes');
    const queryObject = new Parse.Query(Estudante);
    queryObject.find({
      success: function (results) {
        for (let i = 0; i < results.length; i++) {
          const object = results[i];
          aux.push({
            id: object.id,
            nome: object.get('nome'),
            sobrenome: object.get('sobrenome'),
            email: object.get('email')
          });
        }
      },
      error: function (error) {
        alert('Error: ' + error.code + ' ' + error.message);
      }
    });

    return aux;
  }

  editSelector(value: string) {
    const aux = { id: '', nome: '', sobrenome: '', email: '' };
    const Estudante = Parse.Object.extend('cadastroEstudantes');
    const queryObject = new Parse.Query(Estudante);
    queryObject.get(value, {
      success: function (results) {
        const id = results.id;
        const nome = results.get('nome');
        const sobrenome = results.get('sobrenome');
        const email = results.get('email');

        aux.id = id;
        aux.nome = nome;
        aux.sobrenome = sobrenome;
        aux.email = email;

      },
      error: function (object, error) {
        // The object was not retrieved successfully.
        // error is a Parse.Error with an error code and message.
      }
    });

    return aux;

  }

  handleAtualizar() {
    const nome = this.estudante.nome;
    const sobrenome = this.estudante.sobrenome;
    const email = this.estudante.email;

    const Estudante = Parse.Object.extend('cadastroEstudantes');
    const queryObject = new Parse.Query(Estudante);
    queryObject.get(this.estudante.id, {
      success: function (object) {
        // The object was retrieved successfully.

        object.set('nome', nome);
        object.set('sobrenome', sobrenome);
        object.set('email', email);
        object.save();
        location.reload();
      },
      error: function (object, error) {
        // The object was not retrieved successfully.
        // error is a Parse.Error with an error code and message.
      }
    });
  }

  handleEdit(value: string) {
    const aux = this.editSelector(value);
    this.estudante = aux;
  }

  handleDelete(value: string) {
    const Estudante = Parse.Object.extend('cadastroEstudantes');
    const queryObject = new Parse.Query(Estudante);
    queryObject.get(value, {
      success: function (object) {
        // The object was retrieved successfully.
        object.destroy({
          success: function (myObject) {
            // The object was deleted from the Parse Cloud.
            alert('Estudante deleted with success!');
            location.reload();

          },
          error: function (myObject, error) {
            // The delete failed.
            // error is a Parse.Error with an error code and message.
            alert('Mensagem de erro: ' + error.message);
          }
        });
      },
      error: function (object, error) {
        // The object was not retrieved successfully.
        // error is a Parse.Error with an error code and message.
      }
    });

    // this.load();
  }

  handleSubmit() {
    const Estudante = Parse.Object.extend('cadastroEstudantes');
    const estudante = new Estudante();
    estudante.set('nome', this.estudante.nome);
    estudante.set('sobrenome', this.estudante.sobrenome);
    estudante.set('email', this.estudante.email);

    estudante.save(null, {
      success: function (estudante) {
        alert('Estudante created with success!');
        location.reload();
      },
      error: function (response, error) {
        alert('Mensagem de erro: ' + error.message);
      }
    });

  }

  handleNomeChange = (event: KeyboardEvent) => {
    this.estudante.nome = (<HTMLInputElement>event.target).value;
  }

  handleSobrenomeChange = (event: KeyboardEvent) => {
    this.estudante.sobrenome = (<HTMLInputElement>event.target).value;
  }
  handleEmailChange = (event: KeyboardEvent) => {
    this.estudante.email = (<HTMLInputElement>event.target).value;
  }


}
