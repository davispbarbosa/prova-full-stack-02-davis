import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroProvaComponent } from './cadastro-prova.component';

describe('CadastroProvaComponent', () => {
  let component: CadastroProvaComponent;
  let fixture: ComponentFixture<CadastroProvaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroProvaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroProvaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
