import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { environment } from './../environments/environment';

import { AppComponent } from './app.component';
import { CadastroProvaComponent } from './cadastro-prova/cadastro-prova.component';
// import { CadastroEstudantesComponent } from './cadastro-estudantes/cadastro-estudantes.component';
import { CadastroAlternativasComponent } from './cadastro-alternativas/cadastro-alternativas.component';
import { CadastroItensComponent } from './cadastro-itens/cadastro-itens.component';
import { ResultadoEstudantesComponent } from './resultado-estudantes/resultado-estudantes.component';
import { ListaEstudantesComponent } from './lista-estudantes/lista-estudantes.component';
import { EstudantesComponent } from './estudantes/estudantes.component';

const appRoutes: Routes = [
  { path: 'app-cadastro-prova', component: CadastroProvaComponent },
  { path: 'app-estudantes', component: EstudantesComponent },
  { path: 'app-cadastro-itens', component: CadastroItensComponent },
  { path: 'app-cadastro-alternativas', component: CadastroAlternativasComponent },
  { path: 'app-resultado-estudantes', component: ResultadoEstudantesComponent },
  { path: 'app-lista-estudantes', component: ListaEstudantesComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CadastroProvaComponent,
    // CadastroEstudantesComponent,
    CadastroAlternativasComponent,
    CadastroItensComponent,
    ResultadoEstudantesComponent,
    ListaEstudantesComponent,
    EstudantesComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ],
  schemas: []
})
export class AppModule {
}
