import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaEstudantesComponent } from './lista-estudantes.component';

describe('ListaEstudantesComponent', () => {
  let component: ListaEstudantesComponent;
  let fixture: ComponentFixture<ListaEstudantesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaEstudantesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaEstudantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
