import { Component, enableProdMode } from '@angular/core';
import { environment } from './../../environments/environment';
import { Location } from '@angular/common';

declare const Parse: any;

@Component({
  selector: 'app-lista-estudantes',
  templateUrl: './lista-estudantes.component.html',
  styleUrls: ['./lista-estudantes.component.css']
})
export class ListaEstudantesComponent {

  title = 'Lista de Estudantes';
  Estudantes = [{
    id: '',
    nome: '',
    sobrenome: '',
    email: ''
  }];

  Estudante = Parse.Object.extend('cadastroEstudantes');

  constructor(private location: Location) {
    Parse.initialize(environment.PARSE_APP_ID, environment.PARSE_JS_KEY);
    Parse.serverURL = environment.serverURL;
    this.Estudantes = this.listaEstudantes();
  }
  load() {
    location.reload();
  }

  listaEstudantes() {
    const aux = [];
    const queryObject = new Parse.Query(this.Estudante);
    queryObject.find({
      success: function (results) {
        for (let i = 0; i < results.length; i++) {
          const object = results[i];
          aux.push({
            id: object.id,
            nome: object.get('nome'),
            sobrenome: object.get('sobrenome'),
            email: object.get('email')
          });
        }
      },
      error: function (error) {
        alert('Error: ' + error.code + ' ' + error.message);
      }
    });

    return aux;
  }

  delete(value: string) {
    const queryObject = new Parse.Query(this.Estudante);
    queryObject.get(value, {
      success: function (object) {
        // The object was retrieved successfully.
        object.destroy({
          success: function (myObject) {
            // The object was deleted from the Parse Cloud.
            alert('Estudante deleted with success!');
            location.reload();

          },
          error: function (myObject, error) {
            // The delete failed.
            // error is a Parse.Error with an error code and message.
            alert('Mensagem de erro: ' + error.message);
          }
        });
      },
      error: function (object, error) {
        // The object was not retrieved successfully.
        // error is a Parse.Error with an error code and message.
      }
    });

    // this.load();
  }

}
