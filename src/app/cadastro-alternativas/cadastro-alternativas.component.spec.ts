import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroAlternativasComponent } from './cadastro-alternativas.component';

describe('CadastroAlternativasComponent', () => {
  let component: CadastroAlternativasComponent;
  let fixture: ComponentFixture<CadastroAlternativasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroAlternativasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroAlternativasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
