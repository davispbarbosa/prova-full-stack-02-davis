import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroEstudantesComponent } from './cadastro-estudantes.component';

describe('CadastroEstudantesComponent', () => {
  let component: CadastroEstudantesComponent;
  let fixture: ComponentFixture<CadastroEstudantesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroEstudantesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroEstudantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
