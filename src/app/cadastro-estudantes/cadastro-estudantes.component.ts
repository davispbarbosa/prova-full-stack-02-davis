import { Component } from '@angular/core';
import { environment } from './../../environments/environment';
import { Location } from '@angular/common';

declare const Parse: any;

@Component({
  selector: 'app-cadastro-estudantes',
  templateUrl: './cadastro-estudantes.component.html',
  styleUrls: ['./cadastro-estudantes.component.css']
})
export class CadastroEstudantesComponent {
  nome = '';
  sobrenome = '';
  email = '';
  foto = '';


  constructor(private location: Location) {
    Parse.initialize(environment.PARSE_APP_ID, environment.PARSE_JS_KEY);
    Parse.serverURL = environment.serverURL;
  }


  handleSubmit = () => {
    const Estudante = Parse.Object.extend('cadastroEstudantes');
    const estudante = new Estudante();
    estudante.set('nome', this.nome);
    estudante.set('sobrenome', this.sobrenome);
    estudante.set('email', this.email);

    estudante.save(null, {
      success: function (estudante) {
        alert('Estudante created with success!');
      },
      error: function (response, error) {
        alert('Mensagem de erro: ' + error.message);
      }
    });

  }

  handleNomeChange = (event: KeyboardEvent) => {
    this.nome = (<HTMLInputElement>event.target).value;
  }

  handleSobrenomeChange = (event: KeyboardEvent) => {
    this.sobrenome = (<HTMLInputElement>event.target).value;
  }
  handleEmailChange = (event: KeyboardEvent) => {
    this.email = (<HTMLInputElement>event.target).value;
  }
  handleFotoChange = (event: KeyboardEvent) => {
    this.foto = (<HTMLInputElement>event.target).value;
  }




}
