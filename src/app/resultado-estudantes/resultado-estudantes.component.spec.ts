import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadoEstudantesComponent } from './resultado-estudantes.component';

describe('ResultadoEstudantesComponent', () => {
  let component: ResultadoEstudantesComponent;
  let fixture: ComponentFixture<ResultadoEstudantesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultadoEstudantesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadoEstudantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
